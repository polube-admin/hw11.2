import React, { useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import styles from '../styles/Counter.module.css';
import { openBasket } from '../script';
import { closeBasket } from '../script';
import { Basket } from './Basket';
import products from "../data/dataset";
import { selectCount, selectCash } from '../reducers/reposReducer'

function Catalog() {

    const dispatch = useDispatch();
    const [incrementAmount, setIncrementAmount] = useState(2);
    const incrementValue = Number(incrementAmount) || 0;

    const addItem = (id) => {
        return (
            dispatch({ type: "ADD_COUNT", payload: id }) 
        )
    }

    const onChange = (e) => {
        // const { theValue } = e.target.value;
        // setValue(value + theValue);
        // the = Number(e.target.value);
        setIncrementAmount(Math.abs(e.target.value))
        console.log(incrementAmount)
    }

    const minItem = (id) => {
        dispatch({ type: "MIN_COUNT", payload: id })
    }

    const deleteItem = (id) => {
        dispatch({ type: "DEL_ITEM", payload: id })
    }

    const byAmount = (id, theAmount) => {
        dispatch({ type: "BY_AMOUNT", payload: id, amount: theAmount })
    }


    const count = useSelector(selectCount);
    const cash = useSelector(selectCash);


    return (
        <>
            <div className="catalog">
                <div className="wrapper">
                    {products.map(product => {

                        return (
                            <div key={product.id} className="grid">
                                <a href={product.image}><img src={product.image} alt="" /></a>
                                <div className="grid__title">
                                    {product.title}
                                </div>
                                <div className="grid__price">
                                    {product.price}$
                                </div>
                                <div className="grid__input">
                                    <input
                                        className={styles.textbox}
                                        aria-label="Set increment amount"
                                        type="number"
                                        placeholder="Скільки?"
                                        onChange={onChange}

                                    />
                                    <button
                                        className={styles.button}
                                        onClick={() => {byAmount(product.id, incrementValue)}}
                                    >
                                        Добавити шт
                                    </button>
                                </div>
                                <div className="grid__button">
                                    <button
                                        className={styles.button}
                                        aria-label="Increment value"
                                        onClick={() => { addItem(product.id) }}
                                    >
                                        Добавити
                                    </button>
                                </div>
                            </div>
                        )
                    })}
                </div>
            </div>
            <Basket cash={cash} count={count} open={openBasket} close={closeBasket} add={addItem} min={minItem} del={deleteItem} />
        </>
    )



}

export default Catalog;