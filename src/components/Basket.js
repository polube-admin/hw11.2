/* eslint-disable array-callback-return */
import styles from '../styles/Basket.module.css';
import React from 'react';
import products from "../data/dataset";


// import arr from '../reducers/reposReducer'

export function Basket(props) {

    return (
        <>
            <button className={styles.basket_icon} onClick={props.open}>
                <i className={styles.fas}>К</i>
                <div className={styles.basket_icon__number}>
                    <span className={styles.cart__bold}>{props.count}</span>
                </div>
            </button>
            <div className={styles.basket} id="basket">
                <div className={styles.basket__inner}>
                    <div className={styles.basket__inner__box}>
                        <div onClick={props.close} id="closerBasket" className={styles.basket_closer}><span className={styles.closer}>X</span></div>

                        <div className={styles.basket__inner__title}>
                            Корзина
                        </div>
                        <div className={styles.cart}>
                            <form className={styles.form}>
                                <div className={styles.cart__items}>

                                    {products.map(product => {

                                        if (product.count > 0) {

                                            return (
                                                <div className={styles.cart_item}>
                                                    <div className={styles.cart_item__main}>
                                                        <div className={styles.cart_item__start}>
                                                            <button className={styles.cart_item__btn} onClick={() => props.del(product.id)} type="button">X</button>
                                                        </div>
                                                        <div className={styles.cart_item__img_wrapper}>
                                                            <img className={styles.cart_item__img} src={product.image} alt="" />
                                                        </div>
                                                        <div className={styles.cart_item__content}>
                                                            <h3 className={styles.cart_item__title}>{product.title}</h3>
                                                        </div>
                                                    </div>
                                                    <div className={styles.cart_item__end}>
                                                        <div className={styles.cart_item__actions}>
                                                            <button className={styles.cart_item__btn} onClick={() => props.min(product.id)}  type="button">-</button>
                                                            <span className={styles.cart_item__quantity}>{product.count}</span>
                                                            <button className={styles.cart_item__btn} onClick={() => props.add(product.id)} type="button">+</button>
                                                        </div>
                                                        <p className={styles.cart_item__price}>{product.price}$</p>
                                                    </div>
                                                </div>
                                            )

                                        } 



                                    })}

                                    {/*  onClick={props.min(product.id)} */}


                                </div>
                                <div className={styles.basket__inner__cash}>
                                    <div className={styles.basket__inner__cash__left}>
                                        <p className={styles.basket__inner__cash__text}>Всього: </p> <span className={styles.cart__bold}><span
                                            className={styles.summ}>{props.cash.toFixed(1)} $</span>
                                        </span>
                                    </div>
                                    <div className={styles.basket__inner__cash__right}>
                                        <button className={styles.buy}>Замовити</button>
                                    </div>
                                </div>
                            </form>


                        </div>
                    </div>

                </div>
            </div>
        </>)
}
