import products from "../data/dataset.json";
const ADD_ITEM = "ADD_ITEM";
const ADD_COUNT = "ADD_COUNT";
const MIN_COUNT = "MIN_COUNT";
const DEL_ITEM = "DEL_ITEM";
const BY_AMOUNT = "BY_AMOUNT";



const defaultState = {
    products: []
};




export default function reposReducer(state = defaultState, action) {
    switch (action.type) {
        // case INIT:
        //     return {
        //         ...state,
        //         ...state = products,
        //     };
        case ADD_COUNT:
            let out;
            function add() {
                products.forEach(item => {
                    if (item.id === action.payload) {
                        item.count += 1
                        out = item;
                    }

                })
            }
            add();

            for (let i = 0; i < state.products.length; i++) {
                if (state.products[i].id === action.payload) {
                    return {
                        ...state,
                        ...state.products[i].id
                    }
                }
            }

            return {
                ...state,
                ...state.products.push(out)
            }


        case MIN_COUNT: {
            let out;
            function add() {
                products.forEach(item => {
                    if (item.id === action.payload) {
                        item.count -= 1
                        out = item;
                    }
                })
            }
            add();

            for (let i = 0; i < state.products.length; i++) {
                if (state.products[i].id === action.payload) {
                    return {
                        ...state,
                        ...state.products[i].id
                    }
                }
            }

            return {
                ...state
            }
        }

        case DEL_ITEM: {
            let out;
            function del() {
                products.forEach(item => {
                    if (item.id === action.payload) {
                        item.count = 0
                        out = item;
                    }

                })
            }
            del();

            for (let i = 0; i < state.products.length; i++) {
                if (state.products[i].id === action.payload) {
                    return {
                        ...state,
                        ...state.products[i].id
                    }
                }
            }

            return {
                ...state
            }
        }

        case BY_AMOUNT: {
            let out;
            function add() {
                products.forEach(item => {
                    if (item.id === action.payload) {
                        item.count = item.count + action.amount
                        out = item;
                    }

                })
            }
            add();

            for (let i = 0; i < state.products.length; i++) {
                if (state.products[i].id === action.payload) {
                    return {
                        ...state,
                        ...state.products[i].count + action.amount
                    }
                }
            }

            return {
                ...state,
                ...state.products.push(out)
            }
        }

        default: {
            return state;
        }
    }
}


export const selectCount = (state) => {
    let summ = 0;
    for (let i = 0; i < state.repos.products.length; i++) {
        summ += state.repos.products[i].count
    }
    return Number(summ);
};

export const selectCash = (state) => {
    let summ = 0;
    for (let i = 0; i < state.repos.products.length; i++) {
        summ += state.repos.products[i].price
        summ = summ * state.repos.products[i].count
    }
    return Number(summ);
};




export function addCount(id) {
    let out = { type: ADD_COUNT, payload: id };
    return out;
}

export function minCount(id) {
    let out = { type: MIN_COUNT, payload: id };
    return out;
}

export function byAmount(theId, count) {
    let out = { type: BY_AMOUNT, payload: count, id: theId };
    return out;
}
export const arr = (state) => state.repos.products



export const addItem = (item) => ({ type: ADD_ITEM, payload: item });


